import UIKit
import WebKit

class MentalHealthResourcesViewController: SuperViewController,
                                           WKNavigationDelegate
{
  let webView = WKWebView()
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
                                                   
    webView.frame =  self.view.frame
    webView.navigationDelegate = self
    webView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    self.view.addSubview(webView)
  }
  
  override func viewWillAppear(_ animated: Bool)
  {
    super.viewWillAppear(animated)
    
    let filePath = Bundle.main.url(forResource: "mental_health_resources", withExtension: "html")
    let urlRequest = URLRequest.init(url: filePath!)
    webView.load(urlRequest)
  }
  
  func webView(_ webView: WKWebView,
               decidePolicyFor navigationAction: WKNavigationAction,
               decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
  {
    if navigationAction.navigationType == .linkActivated
    {
      if let url = navigationAction.request.url,
        UIApplication.shared.canOpenURL(url)
      {
        UIApplication.shared.open(url)
        decisionHandler(.cancel)
      }
    }
    else
    {
      decisionHandler(.allow)
    }
  }
}
