import UIKit

class SuperViewController: UIViewController
{
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    view.backgroundColor = .white
    
    navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu"),
                                                        style: .plain,
                                                        target: self,
                                                        action: #selector(openSideMenu))
  }
  
  @objc func openSideMenu()
  {
    sideMenuController?.revealMenu()
  }
}
