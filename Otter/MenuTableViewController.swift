import UIKit

class MenuTableViewController: UITableViewController {
  
  let kUserCellIdentifier = "cell"
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
  }
  
  // MARK: - Table view data source, delegates
  
  override func numberOfSections(in tableView: UITableView)
    -> Int
  {
    return 1
  }
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int)
    -> Int
  {
    return 8
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath)
    -> UITableViewCell
  {
    let cell = UITableViewCell(style: .default, reuseIdentifier: kUserCellIdentifier)
    
    switch (indexPath.row)
    {
    case 0:
      cell.textLabel?.text = "Mental Health Resources"
      
    case 1:
      cell.textLabel?.text = "Account Details"
      
    case 2:
      cell.textLabel?.text = "Account Settings"
      
    case 3:
      cell.textLabel?.text = "Tecnical Support"
      
    case 4:
      cell.textLabel?.text = "About Otter"
      
    case 5:
      cell.textLabel?.text = "Contact"
      
    case 6:
      cell.textLabel?.text = "Privacy"
      
    case 7:
      cell.textLabel?.text = "FAQ's"
    
    default: break
    }
    
    return cell
  }
  
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
  {
    tableView.deselectRow(at: indexPath, animated: true)
  }
}
