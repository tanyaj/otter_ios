import UIKit
import fluid_slider

class ScaleSelectorView: UIView
{
  let question: UILabel =
  {
    let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 10.0, height: 0.0))
    label.autoresizingMask = [.flexibleWidth]
    label.textColor = .darkGray
    label.font = UIFont.systemFont(ofSize: 18.0, weight: .regular)
    label.numberOfLines = 0
    return label
  }()
  
  let first: UILabel =
  {
    let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 10.0, height: 0.0))
    label.textColor = .darkGray
    label.font = UIFont.systemFont(ofSize: 12.0, weight: .light)
    label.text = "APPLIES TO ME"
    return label
  }()
  
  let last: UILabel =
  {
    let label = UILabel(frame: CGRect(x: 0.0, y: 0.0, width: 10.0, height: 0.0))
    label.textColor = .darkGray
    label.textAlignment = .right
    label.font = UIFont.systemFont(ofSize: 12.0, weight: .light)
    label.text = "DOES NOT APPLY TO ME"
    return label
  }()
  
//  var value: Int
//  {
//    get { 0 }
//    
//    set {}
//  }
  
  // MARK: - Memory life cycle
  
  func add(_ q: String)
  {
    isUserInteractionEnabled = true
    
    //
    question.frame = CGRect(x: 0.0, y: 0.0, width: bounds.width, height: 10.0)
    question.text = q
    addSubview(question)
    
    question.sizeToFit()
    
    let min = "1"
    let max = "20"
    
    let slider = Slider()
    slider.frame = CGRect(x: 0.0, y: question.frame.maxY + 15.0, width: bounds.width, height: 44.0)
    slider.attributedTextForFraction = { fraction in
      let formatter = NumberFormatter()
      formatter.maximumIntegerDigits = 2
      formatter.maximumFractionDigits = 0
      let string = formatter.string(from: (fraction * 20) as NSNumber) ?? ""
      return NSAttributedString(string: string == "0" ? min : string)
    }
    slider.setMinimumLabelAttributedText(NSAttributedString(string: min,
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white,
                                                                         NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18.0, weight: .bold)]))
    slider.setMaximumLabelAttributedText(NSAttributedString(string: max,
                                                            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white,
                                                                         NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18.0, weight: .bold)]))
    slider.fraction = 0.0
    slider.shadowOffset = CGSize(width: 0, height: 5)
    slider.shadowBlur = 5
    slider.shadowColor = UIColor(white: 0, alpha: 0.1)
    slider.startGradientColor = UIColor.Karry
    slider.endGradientColor = UIColor.Peanut
    slider.contentViewColor = UIColor.SeaBlue
    slider.valueViewColor = .white
    
    slider.didBeginTracking = { [weak self] _ in
      self?.question.alpha = 0.5
    }
    slider.didEndTracking = { [weak self] _ in
      self?.question.alpha = 1.0
    }
    
    addSubview(slider)
    
    //
    first.frame = CGRect(x: 0.0, y: slider.frame.maxY, width: bounds.size.width, height: 18.0)
    addSubview(first)
    
    //
    last.frame = CGRect(x: 0.0, y: slider.frame.maxY, width: bounds.size.width, height: 18.0)
    addSubview(last)
    
    //
    self.frame = CGRect(x: frame.minX,
                        y: frame.minY,
                        width: bounds.size.width,
                        height: first.frame.maxY)
  }
}
