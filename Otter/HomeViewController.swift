import UIKit

class HomeViewController: SuperViewController
{
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    let margin = CGFloat(10)
    let doubleMargin = margin * 2.0
    let w = view.bounds.size.width - doubleMargin
    
    // title
    let first = UILabel(frame: CGRect(x: doubleMargin, y: margin, width: w - doubleMargin, height: 160.0))
    first.numberOfLines = 0
    first.font = UIFont.systemFont(ofSize: 18.0, weight: .light)
    first.textColor = .black
    first.textAlignment = .center
    first.text = "Your information is completely confidential. Otter will never share your individual scores and will only ever share collective results in a way that can never be traced back to an individual."
    view.addSubview(first)
    
    let second = UILabel(frame: CGRect(x: margin, y: first.frame.maxY, width: w, height: 34.0))
    second.numberOfLines = 0
    second.font = UIFont.systemFont(ofSize: 24.0, weight: .bold)
    second.textColor = .DeepCoffee
    second.textAlignment = .center
    second.text = "Ready to go?"
    view.addSubview(second)
    
    let button:UIButton = UIButton()
    button.frame = CGRect(x: (w - 100) / 2,
                          y: second.frame.maxY + doubleMargin,
                          width: 120.0,
                          height: 44.0)
    button.showsTouchWhenHighlighted = true
    button.setTitle("YES", for: .normal)
    button.backgroundColor = .clear
    button.layer.borderColor = UIColor.blue.cgColor
    button.layer.borderWidth = 1.0
    button.layer.cornerRadius = 9.0
    button.setTitleColor(.blue, for: .normal)
    button.addTarget(self, action: #selector(openQuestions), for: .touchUpInside)
    view.addSubview(button)
    
    let quote = UIImageView(image: UIImage(named: "quote")!)
    quote.autoresizingMask = [.flexibleTopMargin]
    
    let h = (view.bounds.width / quote.image!.size.width) * quote.image!.size.height

    quote.frame = CGRect(x: 0.0, y: view.bounds.height - h - 49.0, width: view.bounds.width, height:h)
    view.addSubview(quote)
  }
  
  @objc func openQuestions()
  {
    let app = UIApplication.shared.delegate as! AppDelegate
    app.openSection(1)
  }
  
  func scale()
  {
    
  }
}
