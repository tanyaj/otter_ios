import UIKit

class ResultsViewController: SuperViewController
{
  override func viewDidLoad()
  {
    super.viewDidLoad()
    
    let margin = CGFloat(10)
    let doubleMargin = margin * 2.0
    let w = view.bounds.size.width - doubleMargin
    
    let scroll = UIScrollView(frame: view.bounds)
    scroll.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    view.addSubview(scroll)
    
    // title
    let first = UILabel(frame: CGRect(x: margin, y: margin, width: w, height: 74.0))
    first.numberOfLines = 0
    first.font = UIFont.systemFont(ofSize: 24.0, weight: .bold)
    first.textColor = .DeepCoffee
    first.textAlignment = .center
    first.text = "Results"
    scroll.addSubview(first)
    
    let size = first.sizeThatFits(CGSize(width: w, height: 74.0))
    first.frame = CGRect(x: margin, y: margin, width: w, height: size.height)
    
    let second = UILabel(frame: CGRect(x: doubleMargin, y: first.frame.maxY + margin, width: w - doubleMargin, height: 200.0))
    second.numberOfLines = 0
    second.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
    second.textColor = .black
    second.text =
    """
    The results tables below will show your emotional measures in Depression, Stress,  Anxiety and overall well being (the Jenkyn scale).
    The Depression scale assesses levels of hopelessness, self-depreciation and lack of interest/involvement.
    The Stress scale assesses difficulty relaxing, and being easily upset or agitated.
    The Anxiety scale assesses situational anxiety, and subjective experiences of anxious affect.
    The Jenkyn scale is a combined overall score.
    
    Keep an eye on your results and if you feel you need help or support with your mental health,
    resources are available by clicking the link icon in the footer.
    """
    
    scroll.addSubview(second)
    
    second.sizeToFit()
    
    // D
    let d = UIImageView(image: UIImage(named: "graph_d")!)

    let w_ = view.bounds.width - (doubleMargin * 2.0)
    let h_ = (w_ / d.image!.size.width) * d.image!.size.height
    
    d.frame = CGRect(x: doubleMargin, y: second.frame.maxY + doubleMargin, width: w_, height: h_)
    scroll.addSubview(d)
    
    // S
    let s = UIImageView(image: UIImage(named: "graph_s")!)
    
    s.frame = CGRect(x: doubleMargin, y: d.frame.maxY + doubleMargin, width: w_, height: h_)
    scroll.addSubview(s)
    
    // A
    let a = UIImageView(image: UIImage(named: "graph_a")!)
    
    a.frame = CGRect(x: doubleMargin, y: s.frame.maxY + doubleMargin, width: w_, height: h_)
    scroll.addSubview(a)
    
    // J
    let j = UIImageView(image: UIImage(named: "graph_j")!)
    
    j.frame = CGRect(x: doubleMargin, y: a.frame.maxY + doubleMargin, width: w_, height: h_)
    scroll.addSubview(j)
    
    scroll.contentSize = CGSize(width: 1.0, height: j.frame.maxY + doubleMargin)
  }
}
