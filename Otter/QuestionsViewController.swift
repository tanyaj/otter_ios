import UIKit

class QuestionsViewController: SuperViewController
{
  override func viewDidLoad()
  {
    super.viewDidLoad()
        
    let margin = CGFloat(10)
    let doubleMargin = margin * 2.0
    let w = view.bounds.size.width - doubleMargin
    
    let scroll = UIScrollView(frame: view.bounds)
    scroll.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    view.addSubview(scroll)
    
    // title
    let first = UILabel(frame: CGRect(x: margin, y: margin, width: w, height: 74.0))
    first.numberOfLines = 0
    first.font = UIFont.systemFont(ofSize: 24.0, weight: .bold)
    first.textColor = .DeepCoffee
    first.textAlignment = .center
    first.text = "What's been happening this week?..."
    scroll.addSubview(first)
    
    let s = first.sizeThatFits(CGSize(width: w, height: 74.0))
    first.frame = CGRect(x: margin, y: margin, width: w, height: s.height)
    
    let second = UILabel(frame: CGRect(x: margin, y: first.frame.maxY + margin, width: w, height: 74.0))
    second.numberOfLines = 0
    second.font = UIFont.systemFont(ofSize: 16.0, weight: .regular)
    second.textColor = .black
    second.textAlignment = .center
    second.text = "Please read each statement and select the number that indicates how much the statement applies to you."
    scroll.addSubview(second)
    
    second.sizeToFit()
    
    let line = UIView(frame: CGRect(x: doubleMargin,
                                    y: second.frame.maxY + doubleMargin,
                                    width: w - doubleMargin,
                                    height: 2.0))
    line.backgroundColor = .Java
    scroll.addSubview(line)
    
    let q = ["Over the past week, I felt that I had lost interest in just about everything",
             "Over the past week, I felt I wasn’t worth much as a person",
             "Over the past week, I just couldn’t seem to get going",
             "Over the past week, I felt scared without any good reason",
             "Over the past week, I was worried about situations in which I might panic and make a fool of myself",
             "Over the past week, I felt I was close to panic",
             "Over the past week, I tended to over-react to situations",
             "Over the past week, I found it hard to wind down",
             "Over the past week, I found myself getting agitated"]
    
    var y = line.frame.maxY + doubleMargin
    
    for var i in q
    {
      let s = ScaleSelectorView(frame: CGRect(x: margin,
                                              y: y,
                                              width: w,
                                              height: 10.0))
      s.add(i)
      scroll.addSubview(s)
      
      y += s.bounds.height + 30.0
    }
    
    let button:UIButton = UIButton()
    button.frame = CGRect(x: (w - 100) / 2,
                          y: y + margin,
                          width: 120.0,
                          height: 44.0)
    button.showsTouchWhenHighlighted = true
    button.setTitle("SUBMIT", for: .normal)
    button.backgroundColor = .clear
    button.layer.borderColor = UIColor.blue.cgColor
    button.layer.borderWidth = 1.0
    button.layer.cornerRadius = 9.0
    button.setTitleColor(.blue, for: .normal)
    scroll.addSubview(button)
    
    scroll.contentSize = CGSize(width: 1.0, height: button.frame.maxY + 30.0)
  }
  
  @objc func openMenu()
  {
    self.sideMenuController?.revealMenu()
  }
}
