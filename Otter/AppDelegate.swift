import UIKit
import SideMenuSwift

@UIApplicationMain
class AppDelegate: UIResponder,
                   UIApplicationDelegate
{
  var window: UIWindow?

  let tabBar = UITabBarController()
  
  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?)
    -> Bool
  {
    tabBar.tabBar.barTintColor = .Java
    tabBar.tabBar.tintColor = .white

    let a = HomeViewController()
    a.tabBarItem = UITabBarItem(title: "Home", image: UIImage(named: "home"), tag: 0)
    
    let b = QuestionsViewController()
    b.tabBarItem = UITabBarItem(title: "Questions", image: UIImage(named: "q"), tag: 1)
    
    let c = ResultsViewController()
    c.tabBarItem = UITabBarItem(title: "Results", image: UIImage(named: "r"), tag: 2)
    
    let d = MentalHealthResourcesViewController()
    d.tabBarItem = UITabBarItem(title: "Links", image: UIImage(named: "links"), tag: 3)
    
    let e = SettingsViewController()
    e.tabBarItem = UITabBarItem(title: "Settings", image: UIImage(named: "settings"), tag: 4)
    
    tabBar.viewControllers = [createNavigationController(a),
                              createNavigationController(b),
                              createNavigationController(c),
                              createNavigationController(d),
                              createNavigationController(e)]
    
    let menuTableViewController = MenuTableViewController()
    
    SideMenuController.preferences.basic.menuWidth = 240.0
    SideMenuController.preferences.basic.direction = .right
    SideMenuController.preferences.basic.enablePanGesture =  false
    
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.rootViewController = SideMenuController(contentViewController: tabBar,
                                                    menuViewController: menuTableViewController)
    window?.makeKeyAndVisible()
    
    return true
  }
  
  func openSection(_ index: Int)
  {
    tabBar.selectedIndex = index
  }
  
  func createNavigationController(_ viewController: UIViewController)
  -> UINavigationController
  {
    let uinc = UINavigationController(rootViewController: viewController)
    uinc.navigationBar.isTranslucent = false
    uinc.navigationBar.barTintColor = .Boulder
    uinc.navigationBar.tintColor = .white
    return uinc
  }
}

