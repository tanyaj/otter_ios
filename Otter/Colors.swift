import UIKit

extension UIColor
{
  static var Java: UIColor
  { #colorLiteral(red: 0.115911983, green: 0.7650413513, blue: 0.7376647592, alpha: 1)
    return UIColor(red: 0.12, green: 0.77, blue: 0.74, alpha: 1.0)
  }
  
  static var BlueGreen: UIColor
  { #colorLiteral(red: 0.1244848892, green: 0.6743208766, blue: 0.7220842838, alpha: 1)
    return UIColor(red: 0.12, green: 0.67, blue: 0.72, alpha: 1.0)
  }
  
  static var Genoa: UIColor
  { #colorLiteral(red: 0.05677336454, green: 0.4385672808, blue: 0.4198474884, alpha: 1)
    return UIColor(red: 0.06, green: 0.44, blue: 0.42, alpha: 1.0)
  }
  
  static var SeaBlue: UIColor
  { #colorLiteral(red: 0.00881325081, green: 0.4381306767, blue: 0.6006012559, alpha: 1)
    return UIColor(red: 0.01, green: 0.44, blue: 0.6, alpha: 1.0)
  }
  
  static var Tarawera: UIColor
  { #colorLiteral(red: 0.007911023684, green: 0.2216992378, blue: 0.3219200969, alpha: 1)
    return UIColor(red: 0.01, green: 0.22, blue: 0.32, alpha: 1.0)
  }
  
  static var ColumbiaBlue: UIColor
  { #colorLiteral(red: 0.5821347237, green: 0.8895437121, blue: 1, alpha: 1)
    return UIColor(red: 0.58, green: 0.89, blue: 1.0, alpha: 1.0)
  }
  
  static var Boulder: UIColor
  { #colorLiteral(red: 0.4823139906, green: 0.4823748469, blue: 0.4822933078, alpha: 1)
    return UIColor(red: 0.48, green: 0.48, blue: 0.48, alpha: 1.0)
  }
  
  static var Iron: UIColor
  { #colorLiteral(red: 0.7960167527, green: 0.7961130738, blue: 0.7959839106, alpha: 1)
    return UIColor(red: 0.80, green: 0.80, blue: 0.80, alpha: 1.0)
  }
  
  static var DeepCoffee: UIColor
  { #colorLiteral(red: 0.4383518696, green: 0.2816852033, blue: 0.2504312992, alpha: 1)
    return UIColor(red: 0.44, green: 0.28, blue: 0.25, alpha: 1.0)
  }
  
  static var Peanut: UIColor
  { #colorLiteral(red: 0.471082747, green: 0.2064935267, blue: 0.06745813787, alpha: 1)
    return UIColor(red: 0.47, green: 0.21, blue: 0.07, alpha: 1.0)
  }
  
  static var Karry: UIColor
  { #colorLiteral(red: 0.9833161235, green: 0.8476666808, blue: 0.7719082236, alpha: 1)
    return UIColor(red: 0.98, green: 0.85, blue: 0.77, alpha: 1.0)
  }
}
